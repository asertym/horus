// Gulp 4 Configuration

var gulp = require('gulp'),
	changed = require('gulp-changed'),
	plumber = require('gulp-plumber'),
	twig = require('gulp-twig'),
	browser = require('browser-sync').create(),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),

	// Directories
	src = {
		scss: 'dev/scss/**/*.scss',
		js: 'dev/js/*.js',
		twig: 'dev/*.html',
		twigs: 'dev/twig/**/*.twig',
		html: '*.html',
		css: 'build/css',
		map: '/maps',
		scripts: 'build/js'
	};

// Notifications
var notify = require('gulp-notify');

// JS minifier
var uglify = require('gulp-uglify');

// Makes the OS notification a bit more useful in case of error
var onError = function (err) {
	notify.onError({
		title: "gulp error in " + err.plugin,
		message: err.toString()
	})(err);
	this.emit('end');
};

// Compile Twig to HTML
gulp.task('twig', function () {
	return gulp.src(src.twig) // source
		.pipe(twig())
		.pipe(gulp.dest('build')); // output destination
});

gulp.task('styles', function () {
	return gulp.src(src.scss) // source
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(changed(src.scss))
		.pipe(sourcemaps.init())
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({

		}))
		.pipe(sourcemaps.write(src.map))
		.pipe(gulp.dest(src.css)) // output
		.pipe(browser.stream())
});

gulp.task('js', function () {
	return gulp.src(src.js)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(changed(src.js))
		.pipe(uglify())
		.pipe(gulp.dest(src.scripts)) // output
		.pipe(browser.stream())
});

// Browsersync
// server & file ward
gulp.task('pack', function () {
	browser.init({
		server: {
			baseDir: "build",
		}
	});
	gulp.watch(src.scss, gulp.series('styles'));
	gulp.watch(src.js, gulp.series('js'));
	gulp.watch([src.twig, src.twigs], gulp.series('twig')).on('change', browser.reload);
});

// launcher
gulp.task('run', gulp.series(gulp.parallel('styles', 'twig', 'js'), 'pack'));